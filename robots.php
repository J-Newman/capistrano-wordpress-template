<?php 
header("Content-Type: text/plain"); 
header("Cache-Control: max-age=0"); 

$host = $_SERVER['HTTP_HOST'];

$staginghosts = array(".earth.",".jupiter.",".local.",".fcsint.");
$re = "/(" . implode("|", $staginghosts) . ")/";
if (preg_match($re, $host) > 0) { 
	include("robots.notlive.txt");
}else{
	include("robots.live.txt");
}
